<?php

namespace BinaryStudioAcademy\Game\Models\Resources;
use BinaryStudioAcademy\Game\Models\Resource;

class Sand extends Resource
{
    public function getResourceName() : string
    {
        return self::SAND;
    }
}
