<?php

namespace BinaryStudioAcademy\Game\Models\Resources;
use BinaryStudioAcademy\Game\Models\Resource;

class Iron extends Resource
{
    public function getResourceName() : string
    {
        return self::IRON;
    }
}
