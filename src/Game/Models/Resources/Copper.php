<?php

namespace BinaryStudioAcademy\Game\Models\Resources;
use BinaryStudioAcademy\Game\Models\Resource;

class Copper extends Resource
{
    public function getResourceName() : string
    {
        return self::COPPER;
    }
}
