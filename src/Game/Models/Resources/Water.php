<?php

namespace BinaryStudioAcademy\Game\Models\Resources;
use BinaryStudioAcademy\Game\Models\Resource;

class Water extends Resource
{
    public function getResourceName() : string
    {
        return self::WATER;
    }
}
