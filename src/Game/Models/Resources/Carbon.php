<?php

namespace BinaryStudioAcademy\Game\Models\Resources;
use BinaryStudioAcademy\Game\Models\Resource;


class Carbon extends Resource
{
    public function getResourceName() : string
    {
        return self::CARBON;
    }
}
