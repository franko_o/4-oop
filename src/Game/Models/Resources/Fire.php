<?php

namespace BinaryStudioAcademy\Game\Models\Resources;
use BinaryStudioAcademy\Game\Models\Resource;

class Fire extends Resource
{
    public function getResourceName() : string
    {
        return self::FIRE;
    }
}
