<?php

namespace BinaryStudioAcademy\Game\Models\Resources;
use BinaryStudioAcademy\Game\Models\Resource;
use BinaryStudioAcademy\Game\Exceptions\GameException;

class Metal extends Resource
{
    public function getResourceName() : string
    {
        return self::METAL;
    }

    public function __construct()
    {
        $this->setValue(0);
    }

    public function produce()
    {
        $this->value += 1;
    }

    public function increaseValue(int $value)
    {
        throw new GameException('Cant increase metal value. Use produce.');
    }

    public function getComposite()
    {
        return [
            Resource::IRON => 1,
            Resource::FIRE => 1
        ];
    }
}
