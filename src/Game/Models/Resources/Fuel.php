<?php

namespace BinaryStudioAcademy\Game\Models\Resources;
use BinaryStudioAcademy\Game\Models\Resource;

class Fuel extends Resource
{
    public function getResourceName() : string
    {
        return self::FUEL;
    }
}
