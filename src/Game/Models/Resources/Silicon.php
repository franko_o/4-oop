<?php

namespace BinaryStudioAcademy\Game\Models\Resources;
use BinaryStudioAcademy\Game\Models\Resource;

class Silicon extends Resource
{
    public function getResourceName() : string
    {
        return self::SILICON;
    }
}
