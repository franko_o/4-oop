<?php

namespace BinaryStudioAcademy\Game\Models;

abstract class Resource
{
    const CARBON = 'carbon';
    const COPPER = 'copper';
    const FIRE = 'fire';
    const FUEL = 'fuel';
    const IRON = 'iron';
    const SAND = 'sand';
    const SILICON = 'silicon';
    const WATER = 'water';
    const METAL = 'metal';

    /**
     * @var int amount
     */
    protected $value;

    public function __construct()
    {
        $this->setValue(0);
    }

    public function increaseValue(int $value)
    {
        $this->value += $value;
    }

    public function decreaseValue(int $value)
    {
        $this->value -= $value;
    }

    public function setValue(int $value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    abstract public function getResourceName() : string;
}
