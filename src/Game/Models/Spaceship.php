<?php

namespace BinaryStudioAcademy\Game\Models;

use BinaryStudioAcademy\Game\Contracts\Module;
use BinaryStudioAcademy\Game\Exceptions\GameException;
use BinaryStudioAcademy\Game\Helpers\Helper;

/**
 * Class Spaceship. Singleton
 * @package BinaryStudioAcademy\Game
 */
class Spaceship
{
    /**
     * Required Modules for spaceship
     */
    const REQUIRED_MODULES = ['ControlUnit', 'Engine', 'Launcher', 'Porthole', 'Shell', 'Tank'];

    /**
     * @var Spaceship
     */
    private static $instance;

    /**
     * @var array
     */
    protected $builtModules = [];

    /**
     * Spaceship instance
     */
    public static function getInstance(): Spaceship
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __construct() { }

    /**
     * @param Module $module
     * @param Store $store
     */
    public function buildModule(Module $module, Store $store)
    {
        if ($this->isCompleted()) {
            throw new GameException('Is completed');
        }
        if ($this->hasModule($module)) {
            $shortName = Helper::getShortClassName($module);
            throw new GameException('Attention! ' . ucfirst($shortName) . ' is ready.');
        }

        // Build Module
        $necessaryResources = $module->getNecessaryResources();
        $resources = $store->checkAvailability($necessaryResources);

        if (! empty($resources)) {
            throw new GameException('Inventory should have: ' . implode(',', $resources) . '.');
        }
        $module->build($store);

        $this->builtModules[] = Helper::getShortClassName($module);
    }

    /**
     * Check if spaceship is completed
     *
     * @return bool
     */
    public function isCompleted() : bool
    {
        $containsSearch = count(array_intersect(self::REQUIRED_MODULES, $this->builtModules)) == count(self::REQUIRED_MODULES);

        return $containsSearch;
    }

    /**
     * Check if spaceship has module
     *
     * @param Module $module
     * @return bool
     */
    public function hasModule(Module $module) : bool
    {
        return in_array( Helper::getShortClassName($module), $this->builtModules);
    }

    /**
     * Show built modules
     * @return array
     */
    public function getReadyModules()
    {
        return $this->builtModules;
    }

    /**
     * Show built modules
     * @return array
     */
    public function getFullModules()
    {
        return self::REQUIRED_MODULES;
    }

    /**
     * Show modules which is empty
     * @return array
     */
    public function getEmptyModules()
    {
        return array_diff(self::REQUIRED_MODULES, $this->builtModules);
    }
}
