<?php

namespace BinaryStudioAcademy\Game\Models;

use BinaryStudioAcademy\Game\Contracts\Module;

abstract class ComplexMaterial extends Resource implements Module
{
    const WIRES = 'wires';
    const IC = 'ic';

    public function getComposite()
    {
        return false;
    }
}
