<?php

namespace BinaryStudioAcademy\Game\Models\Modules;
use BinaryStudioAcademy\Game\Models\Resource;
use BinaryStudioAcademy\Game\Models\Module;

class Porthole extends Module
{
    public function getNecessaryResources() : array
    {
        return [
            Resource::SAND => 1,
            Resource::FIRE => 1
        ];
    }
}
