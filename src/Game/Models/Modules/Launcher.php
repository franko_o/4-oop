<?php

namespace BinaryStudioAcademy\Game\Models\Modules;
use BinaryStudioAcademy\Game\Models\Resource;
use BinaryStudioAcademy\Game\Models\Module;

class Launcher extends Module
{
    public function getNecessaryResources() : array
    {
        return [
            Resource::WATER => 1,
            Resource::FIRE => 1,
            Resource::FUEL => 1
        ];
    }
}
