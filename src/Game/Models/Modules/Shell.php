<?php

namespace BinaryStudioAcademy\Game\Models\Modules;
use BinaryStudioAcademy\Game\Models\Resource;
use BinaryStudioAcademy\Game\Models\Module;

class Shell extends Module
{
    public function getNecessaryResources() : array
    {
        return [
            Resource::METAL => 1,
            Resource::FIRE => 1
        ];
    }
}
