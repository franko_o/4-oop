<?php

namespace BinaryStudioAcademy\Game\Models\Modules;
use BinaryStudioAcademy\Game\Models\ComplexMaterial;
use BinaryStudioAcademy\Game\Models\Module;

class ControlUnit extends Module
{
    public function getNecessaryResources() : array
    {
        return [
            ComplexMaterial::IC => 1,
            ComplexMaterial::WIRES => 1
        ];
    }
}
