<?php

namespace BinaryStudioAcademy\Game\Models\Modules;
use BinaryStudioAcademy\Game\Models\Resource;
use BinaryStudioAcademy\Game\Models\Module;

class Tank extends Module
{
    public function getNecessaryResources() : array
    {
        return [
            Resource::METAL => 1,
            Resource::FUEL => 1
        ];
    }
}
