<?php

namespace BinaryStudioAcademy\Game\Models;
use \BinaryStudioAcademy\Game\Contracts\Module as IModule;
/**
 * Class Module
 * @package BinaryStudioAcademy\Game
 */
abstract class Module implements IModule
{
    /**
     * Get resources for module building
     * @return array
     */
    abstract public function getNecessaryResources() : array;

    /**
     * Build a module
     *
     * @param Store $store
     * @return bool
     */
    public function build(Store $store) : bool
    {
        $resources = $this->getNecessaryResources();

        foreach ($resources as $resource => $count) {
            $store->decreaseResourceAmount($resource, $count);
        }

        return true;
    }
}
