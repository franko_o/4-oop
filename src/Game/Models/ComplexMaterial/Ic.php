<?php

namespace BinaryStudioAcademy\Game\Models\ComplexMaterial;
use BinaryStudioAcademy\Game\Models\ComplexMaterial;
use BinaryStudioAcademy\Game\Models\Store;

class Ic extends ComplexMaterial
{
    public function getNecessaryResources() : array
    {
        return [
            self::METAL => 1,
            self::SILICON => 1
        ];
    }

    public function getResourceName() : string
    {
        return 'ic';
    }

    public function build(Store $store) : bool
    {
        $resources = $this->getNecessaryResources();

        foreach ($resources as $resource => $count) {
            $store->decreaseResourceAmount($resource, $count);
        }

        $this->increaseValue(1);
        $store->setIc($this);

        return true;
    }
}
