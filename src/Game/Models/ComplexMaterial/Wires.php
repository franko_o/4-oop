<?php

namespace BinaryStudioAcademy\Game\Models\ComplexMaterial;
use BinaryStudioAcademy\Game\Models\ComplexMaterial;
use BinaryStudioAcademy\Game\Models\Store;

class Wires extends ComplexMaterial
{
    public function getNecessaryResources() : array
    {
        return [
            self::COPPER => 1,
            self::FIRE => 1
        ];
    }

    public function getResourceName() : string
    {
        return 'wires';
    }

    public function build(Store $store) : bool
    {
        $resources = $this->getNecessaryResources();

        foreach ($resources as $resource => $count) {
            $store->decreaseResourceAmount($resource, $count);
        }

        $this->increaseValue(1);
        $store->setWires($this);

        return true;
    }
}
