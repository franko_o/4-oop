<?php

namespace BinaryStudioAcademy\Game\Models;

use BinaryStudioAcademy\Game\Models\Resources\Carbon;
use BinaryStudioAcademy\Game\Models\Resources\Copper;
use BinaryStudioAcademy\Game\Models\Resources\Fire;
use BinaryStudioAcademy\Game\Models\Resources\Sand;
use BinaryStudioAcademy\Game\Models\Resources\Water;
use BinaryStudioAcademy\Game\Models\Resources\Fuel;
use BinaryStudioAcademy\Game\Models\Resources\Iron;
use BinaryStudioAcademy\Game\Models\Resources\Silicon;
use BinaryStudioAcademy\Game\Models\Resources\Metal;
use BinaryStudioAcademy\Game\Models\ComplexMaterial\Wires;
use BinaryStudioAcademy\Game\Models\ComplexMaterial\Ic;
use BinaryStudioAcademy\Game\Exceptions\GameException;

/**
 * Class Player Store
 * @package BinaryStudioAcademy\Game
 */
class Store
{
    protected $carbon;
    protected $copper;
    protected $fire;
    protected $fuel;
    protected $iron;
    protected $sand;
    protected $silicon;
    protected $water;
    protected $metal;
    protected $wires;
    protected $ic;

    private static $instance;

    public static function getInstance(): Store
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @param $necessaryResources
     * @return array
     */
    public function checkAvailability($necessaryResources) : array
    {
        $need = [];

        foreach ($necessaryResources as $resource => $count) {
            /** @var Resource $currentResource */
            $currentResource = $this->$resource;

            if ($currentResource->getValue() < $count) {
                $need[] = $currentResource->getResourceName();
            }
        }

        return $need;
    }

    /**
     * Increase resource amount
     *
     * @param string $resource
     * @param int $count
     */
    public function increaseResourceAmount(string $resource, int $count = 1)
    {
        if (! isset($this->$resource)) {
            throw new GameException('No such resource.' . PHP_EOL);
        }

        /** @var \BinaryStudioAcademy\Game\Models\Resource $currentResource */
        $currentResource = $this->$resource;
        $currentResource->increaseValue($count);
    }

    /**
     * Decrease resource amount
     *
     * @param string $resource
     * @param int $count
     */
    public function decreaseResourceAmount(string $resource, int $count)
    {
        if (! isset($this->$resource)) {
            throw new GameException('No such resource.');
        }

        /** @var \BinaryStudioAcademy\Game\Models\Resource $currentResource */
        $currentResource = $this->$resource;
        $currentResource->decreaseValue($count);
    }

    /**
     * Decrease resource amount
     *
     * @param string $resource
     */
    public function produce(string $resource)
    {
        if (! isset($this->$resource)) {
            throw new GameException('No such resource.');
        }

        /** @var \BinaryStudioAcademy\Game\Models\ComplexMaterial $currentResource */
        $currentResource = $this->$resource;

        $composites = $currentResource->getComposite();

        $needed = [];
        foreach ($composites as $composite => $count) {
            /** @var \BinaryStudioAcademy\Game\Models\ComplexMaterial $material */
            $material = $this->$composite;
            if ($material->getValue() < $count) {
                $needed[] = $composite;
            }
        }

        if (! empty($needed)) {
            throw new GameException('You need to mine: ' . implode(',', $needed) . '.');
        }

        foreach ($composites as $composite => $count) {
            $this->decreaseResourceAmount($composite, $count);
        }
        $currentResource->produce();
    }

    /**
     * Get store resources
     * @return array
     */
    public function getResourceCollection()
    {
        return [
            Resource::CARBON => $this->getCarbon(),
            Resource::COPPER => $this->getCopper(),
            Resource::FIRE => $this->getFire(),
            Resource::FUEL => $this->getFuel(),
            Resource::IRON => $this->getIron(),
            Resource::SAND => $this->getSand(),
            Resource::SILICON => $this->getSilicon(),
            Resource::WATER => $this->getWater(),
            Resource::METAL => $this->getMetal()
        ];
    }

    /****************************************
     *************GETTER/SETTER**************
    ****************************************/

    /*
     * @param Carbon $carbon
     */
    public function setCarbon(Carbon $carbon)
    {
        $this->carbon = $carbon;
    }

    public function getCarbon() : Carbon
    {
        return $this->carbon;
    }

    public function setCopper(Copper $copper)
    {
        $this->copper = $copper;
    }

    public function getCopper() : Copper
    {
        return $this->copper;
    }

    public function setFire(Fire $fire)
    {
        $this->fire = $fire;
    }

    public function getFire() : Fire
    {
        return $this->fire;
    }

    public function setFuel(Fuel $fuel)
    {
        $this->fuel = $fuel;
    }

    public function getFuel() : Fuel
    {
        return $this->fuel;
    }

    public function setIron(Iron $iron)
    {
        $this->iron = $iron;
    }

    public function getIron() : Iron
    {
        return $this->iron;
    }

    public function setSand(Sand $sand)
    {
        $this->sand = $sand;
    }

    public function getSand() : Sand
    {
        return $this->sand;
    }

    public function setSilicon(Silicon $silicon)
    {
        $this->silicon = $silicon;
    }

    public function getSilicon() : Silicon
    {
        return $this->silicon;
    }

    public function setWater(Water $water)
    {
        $this->water = $water;
    }

    public function getWater() : Water
    {
        return $this->water;
    }

    public function setMetal(Metal $metal)
    {
        $this->metal = $metal;
    }

    public function getMetal() : Metal
    {
        return $this->metal;
    }

    public function setWires(Wires $wires)
    {
        $this->wires = $wires;
    }

    public function getWires() : Wires
    {
        return $this->wires;
    }

    public function setIc(Ic $ic)
    {
        $this->ic = $ic;
    }

    public function getIc() : Ic
    {
        return $this->ic;
    }
}
