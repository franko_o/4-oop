<?php

namespace BinaryStudioAcademy\Game\Exceptions;

/**
 * Class GameException
 * @package BinaryStudioAcademy\Game
 */
class GameException extends \PHPUnit\Framework\Exception
{
    public function __construct($message = '', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
