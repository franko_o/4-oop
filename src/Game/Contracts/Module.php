<?php

namespace BinaryStudioAcademy\Game\Contracts;

use BinaryStudioAcademy\Game\Models\Store;

interface Module
{
    public function build(Store $store): bool;

    public function getNecessaryResources() : array;
}
