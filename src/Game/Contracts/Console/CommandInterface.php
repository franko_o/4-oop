<?php

namespace BinaryStudioAcademy\Game\Contracts\Console;

interface CommandInterface
{
    public static function execute($params): string;
}
