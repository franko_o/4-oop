<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Models\ComplexMaterial\Ic;
use BinaryStudioAcademy\Game\Models\ComplexMaterial\Wires;
use BinaryStudioAcademy\Game\Models\Resources\Carbon;
use BinaryStudioAcademy\Game\Models\Resources\Copper;
use BinaryStudioAcademy\Game\Models\Resources\Fire;
use BinaryStudioAcademy\Game\Models\Resources\Fuel;
use BinaryStudioAcademy\Game\Models\Resources\Iron;
use BinaryStudioAcademy\Game\Models\Resources\Metal;
use BinaryStudioAcademy\Game\Models\Resources\Sand;
use BinaryStudioAcademy\Game\Models\Resources\Silicon;
use BinaryStudioAcademy\Game\Models\Resources\Water;
use BinaryStudioAcademy\Game\Models\Spaceship;
use BinaryStudioAcademy\Game\Models\Store;

class Game
{
    /**
     * @var GameManager
     */
    protected $gameManager;

    public function __construct()
    {
        $this->gameManager = new GameManager();
        $this->initialGameResources();
    }

    public function start(Reader $reader, Writer $writer): void
    {
        while(true) {
            $writer->writeln("Enter command: ");
            $isFinished = $this->run($reader, $writer);
            if ($isFinished) {
                break;
            }
        }
    }

    public function run(Reader $reader, Writer $writer): bool
    {
        $input = trim($reader->read());
        $params = explode(" ", $input);
        $command = array_shift($params);
        $result = $this->gameManager->command($command);
        $writer->write($result);
        return $this->gameManager->checkGameOver();
    }

    /**
     * Initial game resources
     */
    protected function initialGameResources()
    {
        $spaceship = Spaceship::getInstance();
        $store = Store::getInstance();

        $carbon = new Carbon();
        $copper = new Copper();
        $fire = new Fire();
        $fuel = new Fuel();
        $iron = new Iron();
        $sand = new Sand();
        $silicon = new Silicon();
        $water = new Water();
        $metal = new Metal();
        $wires = new Wires();
        $ic = new Ic();

        $store->setCarbon($carbon);
        $store->setCopper($copper);
        $store->setFire($fire);
        $store->setFuel($fuel);
        $store->setIron($iron);
        $store->setSand($sand);
        $store->setSilicon($silicon);
        $store->setWater($water);
        $store->setMetal($metal);
        $store->setWires($wires);
        $store->setIc($ic);
    }
}
