<?php

namespace BinaryStudioAcademy\Game\Helpers;
use BinaryStudioAcademy\Game\Exceptions\GameException;
/**
 * Class Helper
 * @package BinaryStudioAcademy\Game
 */
class Helper
{
    /**
     * @param $class
     * @return \BinaryStudioAcademy\Game\Contracts\Console\Command | null
     */
    public static function getConsoleClass($class)
    {
        $className = ucfirst($class);

        $fullClass = "\\BinaryStudioAcademy\\Game\\Console\\{$className}";

        if (! class_exists($fullClass)) {
            return null;
        }

        return new $fullClass();
    }

    /**
     * Convert underscore class name to camelcase
     * Ex.:control_unit -> ControlUnit
     *
     * @param string $class
     * @return string
     */
    public static function convertToShortClassName(string $class)
    {
        $classToCamelCase = str_replace('_', '', ucwords($class, '_'));
        return ucfirst($classToCamelCase);
    }

    /**
     * @param $className
     * @return \BinaryStudioAcademy\Game\Contracts\Module
     */
    public static function getModuleClass($className)
    {
        $className = self::convertToShortClassName($className);
        $class = "\\BinaryStudioAcademy\\Game\\Models\\Modules\\{$className}";
        if (! class_exists($class)) {
            $class = "\\BinaryStudioAcademy\\Game\\Models\\ComplexMaterial\\{$className}";
            if (!class_exists($class)) {
                throw new GameException('There is no such spaceship module.');
            }
        }

        /** @var \BinaryStudioAcademy\Game\Contracts\Module $instance */
        $instance = new $class();
        return $instance;
    }

    /**
     * @param $module
     * @return string
     */
    public static function getShortClassName($module)
    {
        $reflect = new \ReflectionClass($module);
        $shortName = $reflect->getShortName();

        return $shortName;
    }
}
