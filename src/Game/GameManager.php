<?php

namespace BinaryStudioAcademy\Game;


use BinaryStudioAcademy\Game\Models\Spaceship;
use BinaryStudioAcademy\Game\Console\ExitCommand;
use BinaryStudioAcademy\Game\Helpers\Helper;

class GameManager
{
    public $availableCommands = [
        'build', 'help', 'mine', 'produce', 'scheme', 'status'
    ];

    /**
     * Flag to check if game is over
     * @var bool
     */
    protected $gameOver = false;

    public function command($command) : string
    {
        $inputsWithParameter = explode(":", $command);

        $class = $inputsWithParameter[0];
        $parameter = isset($inputsWithParameter[1]) ? $inputsWithParameter[1] : null;

        if ($command === 'exit') {
            $this->gameOver = true;
            return ExitCommand::execute();
        }

        $consoleClass = Helper::getConsoleClass($class);

        if (in_array($class, $this->availableCommands)) {
            return $consoleClass::execute($parameter);
        }

        return 'There is no command ' . $command;
    }

    /**
     * Check is game over
     * @return bool
     */
    public function checkGameOver()
    {
        return $this->gameOver || Spaceship::getInstance()->isCompleted();
    }
}
