<?php

namespace BinaryStudioAcademy\Game\Console;
use BinaryStudioAcademy\Game\Models\Spaceship;
use BinaryStudioAcademy\Game\Models\Store;

/**
 * Class Status
 * @package BinaryStudioAcademy\Game\Console
 */
class Status extends Command
{
    /**
     * @param null $params
     * @return string
     */
    public static function execute($params = null) : string
    {
        $spaceship = Spaceship::getInstance();

        $message = '----Parts ready----'. PHP_EOL;
        $message .= implode(', ', $spaceship->getReadyModules()) . PHP_EOL . PHP_EOL;
        $message .= '----Parts to build----'. PHP_EOL;
        $message .= implode(', ', $spaceship->getEmptyModules()) . PHP_EOL . PHP_EOL;
        $message .= '----Store Resources----'. PHP_EOL;

        $storeCollection = Store::getInstance()->getResourceCollection();
        foreach ($storeCollection as $resource => $object) {
            $message .= ucfirst($resource) . ':' . $object->getValue() . PHP_EOL;
        }

        return $message;
    }
}
