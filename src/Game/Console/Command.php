<?php

namespace BinaryStudioAcademy\Game\Console;
use BinaryStudioAcademy\Game\Contracts\Console\CommandInterface;

/**
 * Class Command
 * @package BinaryStudioAcademy\Game
 */
abstract class Command implements CommandInterface
{
    /**
     * Check if parameter is empty
     *
     * @param $params
     * @return bool|string
     */
    public static function checkParameter($params)
    {
        if (empty($params)) {
            return "Wrong command. Check 'help' for command list";
        }
        return true;
    }
}
