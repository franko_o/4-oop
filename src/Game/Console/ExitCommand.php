<?php

namespace BinaryStudioAcademy\Game\Console;

/**
 * Class ExitCommand
 * @package BinaryStudioAcademy\Game\Console
 */
class ExitCommand extends Command
{
    /**
     * @param null $params
     * @return string
     */
    public static function execute($params = null) : string
    {
        return 'Game Over!';
    }
}
