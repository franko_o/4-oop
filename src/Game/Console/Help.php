<?php

namespace BinaryStudioAcademy\Game\Console;

/**
 * Class Help
 * @package BinaryStudioAcademy\Game\Console
 */
class Help extends Command
{
    /**
     * @param null $params
     * @return string
     */
    public static function execute($params = null) : string
    {
        $commands = [
            [
                'name' => 'status',
                'description' => 'Показывает информацию о кол-ве доступных ресурсов и какие части корабля еще не собраны.'
            ],
            [
                'name' => 'build:<spaceship_module>',
                'description' => 'Построить модуль корабля.'
            ],
            [
                'name' => 'scheme:<spaceship_module>',
                'description' => 'Вывести информацию/схему о том какие модули/ресурсы нужны, чтобы построить модуль. '
            ],
            [
                'name' => 'mine:<resource_name>',
                'description' => 'Добавить единицу ресурса(fire) в инвентарь/склад.'
            ],
            [
                'name' => 'produce:<combined_resource>',
                'description' => 'произвести комбинированный ресурс(метал).'
            ],
            [
                'name' => 'exit',
                'description' => 'Выход из игры'
            ]
        ];

        $message = '';

        foreach ($commands as $command) {
            $length = strlen($command['name']);

            // TODO: edit this bullshit
            if ($length < 7) {
                $tabs = "\t\t\t\t\t";
            } elseif ($length >= 7 && $length <= 21) {
                $tabs = "\t\t\t";
            } else {
                $tabs = "\t\t";
            }
            $message .= $command['name'] . $tabs . $command['description'] . PHP_EOL;
        }

        return $message;
    }
}
