<?php

namespace BinaryStudioAcademy\Game\Console;

use BinaryStudioAcademy\Game\Exceptions\GameException;
use BinaryStudioAcademy\Game\Helpers\Helper;

/**
 * Class Scheme
 * @package BinaryStudioAcademy\Game\Console
 */
class Scheme extends Command
{
    /**
     * @param $spaceshipModule
     * @return string
     */
    public static function execute($spaceshipModule) : string
    {
        if (($result = self::checkParameter($spaceshipModule)) !== true) {
            return $result;
        }

        try {

            $moduleInstance = Helper::getModuleClass($spaceshipModule);
            $resources = $moduleInstance->getNecessaryResources();

            return ucfirst($spaceshipModule) . ' => ' . implode('|', array_keys($resources)) . '.';

        } catch (GameException $ex) {
            return $ex->getMessage();
        }
    }
}
