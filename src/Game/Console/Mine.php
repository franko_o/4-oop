<?php

namespace BinaryStudioAcademy\Game\Console;

use BinaryStudioAcademy\Game\Models\Store;
use BinaryStudioAcademy\Game\Exceptions\GameException;

/**
 * Class Mine
 * @package BinaryStudioAcademy\Game\Console
 */
class Mine extends Command
{
    /**
     * @param $resourceName
     * @return string
     */
    public static function execute($resourceName) : string
    {
        if (($result = self::checkParameter($resourceName)) !== true) {
            return $result;
        }

        try {
            Store::getInstance()->increaseResourceAmount($resourceName);
        } catch (GameException $ex) {
            return $ex->getMessage();
        }

        return ucfirst($resourceName) . ' added to inventory.';
    }
}
