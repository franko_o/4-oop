<?php

namespace BinaryStudioAcademy\Game\Console;
use BinaryStudioAcademy\Game\Models\Spaceship;
use BinaryStudioAcademy\Game\Models\Store;
use BinaryStudioAcademy\Game\Exceptions\GameException;
use BinaryStudioAcademy\Game\Helpers\Helper;

/**
 * Class Build
 * @package BinaryStudioAcademy\Game
 */
class Build extends Command
{
    /**
     * Build spaceship module
     * @param string|null $spaceshipModule
     * @return string
     */
    public static function execute($spaceshipModule) : string
    {
        if (($result = self::checkParameter($spaceshipModule)) !== true) {
            return $result;
        }
        $spaceshipModule = ucfirst($spaceshipModule);

        $spaceship = Spaceship::getInstance();
        $store = Store::getInstance();

        try {

            $moduleInstance = Helper::getModuleClass($spaceshipModule);
            $spaceship->buildModule($moduleInstance, $store);

        } catch (GameException $ex) {
            return $ex->getMessage();
        }

        $message = $spaceshipModule . " is ready!";

        if ($spaceship->isCompleted()) {
            $message .= " => You won!";
        }

        return $message;
    }
}
