<?php

namespace BinaryStudioAcademy\Game\Console;

use BinaryStudioAcademy\Game\Models\Store;
use BinaryStudioAcademy\Game\Exceptions\GameException;

/**
 * Class Produce
 * @package BinaryStudioAcademy\Game\Console
 */
class Produce extends Command
{
    /**
     * @param $combinedResource
     * @return string
     */
    public static function execute($combinedResource) : string
    {
        if (($result = self::checkParameter($combinedResource)) !== true) {
            return $result;
        }

        try {

            Store::getInstance()->produce($combinedResource);

        } catch (GameException $ex) {
            return $ex->getMessage();
        }

        return ucfirst($combinedResource) . ' added to inventory.';
    }
}
